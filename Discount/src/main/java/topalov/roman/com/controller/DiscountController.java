package topalov.roman.com.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class DiscountController {

    @GetMapping("/discount/{productType}")
    public Discount getProductDiscount(@PathVariable("productType") String productType) {
        Discount discount = new Discount();
        discount.setName("discount_" + productType);
        discount.setPercentage(15);
        return discount;
    }
}
