package topalov.roman.com.externalServices.feignDiscount;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import topalov.roman.com.externalServices.Discount;

import static java.lang.String.format;

@Service
@AllArgsConstructor
public class FeignDiscountService {

    private final FeignDiscountApi feignDiscountApi;

    public Discount getDiscountByType(String productType) {
        ResponseEntity<Discount> productDiscount = feignDiscountApi.getProductDiscount(productType);
        if(productDiscount == null || productDiscount.getStatusCode().isError()){
            throw new RuntimeException(format("Failed to access Discount service, product type: ", productType));
        }
        Discount discount = productDiscount.getBody();
        return discount;
    }
}
