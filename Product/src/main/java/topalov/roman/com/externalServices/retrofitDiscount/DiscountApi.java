package topalov.roman.com.externalServices.retrofitDiscount;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import topalov.roman.com.externalServices.Discount;

public interface DiscountApi {

    @GET("/api/discount/{productType}")
    Call<Discount> getProductDiscount(@Path("productType") String productType);
}
