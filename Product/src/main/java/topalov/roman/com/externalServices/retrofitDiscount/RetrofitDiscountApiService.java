package topalov.roman.com.externalServices.retrofitDiscount;

import okhttp3.OkHttpClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import topalov.roman.com.externalServices.Discount;

import java.io.IOException;

import static java.lang.String.format;

@Service
public class RetrofitDiscountApiService {

    private static final String discountUrl = "http://localhost:8085";

    private DiscountApi createDiscountApiCall() {
        OkHttpClient httpClient = new OkHttpClient.Builder().build();
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(httpClient);

        return builder.baseUrl(discountUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(DiscountApi.class);
    }

    public Discount getDiscountByType(String type) {
        Call<Discount> productDiscount = createDiscountApiCall().getProductDiscount(type);
        Response<Discount> execute = null;
        try {
            execute = productDiscount.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (execute == null || !execute.isSuccessful()) {
            throw new RuntimeException(format("Failed to access Discount service, product type: ", type));
        }

        Discount discount = execute.body();
        return discount;
    }

}
