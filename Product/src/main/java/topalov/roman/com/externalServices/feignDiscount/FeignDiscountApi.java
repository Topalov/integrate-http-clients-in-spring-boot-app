package topalov.roman.com.externalServices.feignDiscount;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import topalov.roman.com.externalServices.Discount;

@FeignClient(name = "FeignDiscountApi", url = "http://localhost:8085")
public interface FeignDiscountApi {

    @GetMapping(value = "/api/discount/{productType}")
    ResponseEntity<Discount> getProductDiscount(@PathVariable("productType") String productType);

}


