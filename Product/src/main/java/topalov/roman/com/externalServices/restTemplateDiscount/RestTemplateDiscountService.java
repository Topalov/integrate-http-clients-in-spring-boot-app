package topalov.roman.com.externalServices.restTemplateDiscount;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import topalov.roman.com.externalServices.Discount;

@Service
public class RestTemplateDiscountService {

    private static final String discountUrl = "http://localhost:8085";

    public Discount getDiscountByType(String productType) {

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity requestHttpEntity = new HttpEntity<Void>(new HttpHeaders());

        ResponseEntity<Discount> discountResponseEntity = restTemplate.
                exchange(discountUrl + "/api/discount/" + productType,
                        HttpMethod.GET,
                        requestHttpEntity,
                        Discount.class);
        HttpStatus statusCode = discountResponseEntity.getStatusCode();
        Discount discount = discountResponseEntity.getBody();
        return discount;
    }
}
