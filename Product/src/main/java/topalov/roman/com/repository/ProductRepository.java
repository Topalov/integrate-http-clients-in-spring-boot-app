package topalov.roman.com.repository;

import topalov.roman.com.domain.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductRepository {

    public Product getProductById(String productId) {
        Product product = new Product();
        product.setId(productId);
        product.setName("Golden apple");
        product.setPrice(100);
        product.setType("fruit");
        return product;
    }

}
