package topalov.roman.com.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import topalov.roman.com.domain.Product;
import topalov.roman.com.service.ProductService;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/product/{id}")
    public Product getProductDiscount(@PathVariable("id") String productId) {
        return productService.getProductWithDiscount(productId);
    }

}
