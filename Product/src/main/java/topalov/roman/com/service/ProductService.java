package topalov.roman.com.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import topalov.roman.com.domain.Product;
import topalov.roman.com.externalServices.Discount;
import topalov.roman.com.externalServices.feignDiscount.FeignDiscountService;
import topalov.roman.com.externalServices.restTemplateDiscount.RestTemplateDiscountService;
import topalov.roman.com.externalServices.retrofitDiscount.RetrofitDiscountApiService;
import topalov.roman.com.repository.ProductRepository;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final RetrofitDiscountApiService retrofitDiscountApiService;
    private final RestTemplateDiscountService restTemplateDiscountService;
    private final FeignDiscountService feignDiscountService;


    public Product getProductWithDiscount(String productId) {
        Product product = productRepository.getProductById(productId);

        //Discount discountByType = retrofitDiscountApiService.getDiscountByType(product.getType());
        //Discount discountByType = restTemplateDiscountService.getDiscountByType(product.getType());
        Discount discountByType = feignDiscountService.getDiscountByType(product.getType());

        int newProductPrice = evaluateNewProductPrice(product.getPrice(), discountByType);

        product.setPrice(newProductPrice);
        return product;
    }

    private int evaluateNewProductPrice(int price, Discount discountByType) {
        int discount = discountByType.getPercentage();
        if (discount != 0) {
            price = price - ((price * discount) / 100);
        }
        return price;
    }

}
